// requireds
const colors = require('colors');

const fs = require('fs');

const dir = './tablas';

//Opcion 1:
// module.exports.crearArchivo 


let listarTabla = (base, limite) => {

    console.log('=============='.white);
    console.log(`Tabla de ${base}`.green);
    console.log('=============='.yellow);

    for (let i = 1; i <= limite; i++) {
        console.log(`${base} * ${i} = ${base * i} \n`);
    }
};


let crearArchivo = (base, limite) => {
    return new Promise((resolve, reject) => {
        let data = '';

        if (!Number(base)) {
            reject(`El valor introducido ${base}, no es un Numero`.red);
            return;

        }

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }


        for (let i = 1; i <= limite; i++) {
            data += (`${base} * ${i} = ${base * i} \n`);
        }


        fs.writeFile(`tablas/tabla-${base}`, data, (err) => {
            if (err) reject(err);
            else resolve(`tabla-${base}.txt`.green);
        });

    });
};

//Opcion 2:
module.exports = {
    crearArchivo,
    listarTabla
};