const opts = {
    base: {
        demand: true,
        alias: 'b',
        default: 1
    },
    limite: {
        alias: 'l',
        default: 10
    }
};

const argumento = require('yargs')
    // <comando>, <descripcion>, <opciones>
    .command('listar', 'Imprime en consola la tabla de multiplicar', opts)
    .command('crear', 'Crear un archivo de texto con base y un limite establecido', opts)
    .help()
    .argv;


//<Export argumentos>
module.exports = {
    argumento
};

/**
 * const argumento = require('yargs')
  .command('listar', 'Imprime en consola la tabla de multiplicar', {
        base: {
            demand: true,
            alias: 'b'
        },
        limite: {
            alias: 'l',
            default: 10
        }
    })
    .command('crear', 'Crear un archivo de texto con base y un limite establecido', {
        base: {
            demand: true,
            alias: 'b'
        },
        limite: {
            alias: 'l',
            default: 10
        }
    }) */