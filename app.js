const colors = requiere('colors');

const argumento = require('./config/help').argumento;

const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar.js');


let varComando = argumento._[0];

switch (varComando) {
    case 'listar':
        listarTabla(argumento.base, argumento.limite);
        break;
    case 'crear':
        crearArchivo(argumento.base, argumento.limite)
            .then(archivo => console.log(`Archivo creado ${archivo}`))
            .catch(err => console.log(err));
        break;
    default:
        console.log(`Comando "${varComando}" no es un comando valida, utiliza la ayuda de la aplicacion -> "node app --help"`);
}




//console.log(process);
// console.log(module);